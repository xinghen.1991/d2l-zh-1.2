# -*- coding: utf-8 -*-
"""
@Time    :  2020/12/12
@Author  : xinghen
@File    : nh_webservice.py
"""
import json

from flask import Flask, request, Response

from nh_cal_service import Main

app = Flask("nh-app")


@app.route('/')
def nh_index():
    return 'nh index'

@app.route('/data_cal', methods=['GET'])
def data_cal():
    data_cal_result = Main()
    return Response(data_cal_result, mimetype='application/json')


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8001, debug=True)