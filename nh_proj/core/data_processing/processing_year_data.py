# -*- coding: utf-8 -*-
"""
@Time    :  2020/12/5
@Author  : xinghen
@File    : processing_year_data.py
"""
from typing import Dict

import pandas as pd

from core.data_processing.indicator_dic import IndicatorDic


class NhDataProcessing:
    @staticmethod
    def connect_year_data(data: Dict[str, pd.DataFrame]):
        """
        把当年数据和向前三年的数据连接，向前年度数据缺失则置为空
        :param data:
        :return:
        """
        dim_dic_keys = IndicatorDic.dim_dic.keys()  # all dim key
        data_y_keys = data.keys()
        data_cal = {}  # {年度：{维度：指标数据}}
        year_dim_corr_w = {}  # 每年度-每维度-负相关系数权重

        for _ in data_y_keys:
            con_data = {}  # 连接当前年和向前两年的数据
            for i in range(0, 3):  # 获取年度数据
                _f = str(int(_) - i)
                if str(int(_) - i) in data.keys():
                    year_data = data[_f]
                    # data_cal[_f] = {}
                    for dim_item in dim_dic_keys:
                        dim_indicators = IndicatorDic.dim_dic[dim_item]  # 获取当前维度指标项
                        if dim_item in con_data.keys():
                            con_data[dim_item] = pd.concat([con_data[dim_item],
                                                            year_data.loc[:, dim_indicators['check_indicator']]]
                                                           , axis=0, ignore_index=True)
                        else:
                            con_data[dim_item] = year_data.loc[:, dim_indicators['check_indicator']]
            year_dim_corr_w[_] = {}
            data_cal[_] = con_data
        return data_cal

    @staticmethod
    def every_year_data(data: Dict[str, pd.DataFrame], type):
        """
        将数据组织成，年度，维度的数据
        :param data:
        :return:
        """
        dim_dic_keys = IndicatorDic.dim_dic.keys()  # all dim key
        data_y_keys = data.keys()
        data_cal = {}  # {年度：{维度：指标数据}}
        for _ in data_y_keys:
            year_data = data[_]
            year_dim_data = {}
            for dim_item in dim_dic_keys:
                dim_indicators = IndicatorDic.dim_dic[dim_item]  # 获取当前维度指标项
                inx_set = dim_indicators[type]
                if inx_set:
                    inx_set.add('名称')
                    year_dim_data[dim_item] = pd.DataFrame(year_data.loc[:, inx_set]).set_index('名称')
            data_cal[_] = year_dim_data
        return data_cal

    def every_year_zrbf_data(self, data: Dict[str, pd.DataFrame]):
        """
        将数据组织成，年度，指标的自然禀赋数据
        :param data:
        :return:
        """
        zrbf_keys = list(IndicatorDic.zrbf.keys())
        zrbf_order_list = []
        zrbf_pjxs_list = []
        data_cal = {}  # {年度：{维度：指标数据}}

        for zrbf_key in IndicatorDic.zrbf.keys():
            if IndicatorDic.zrbf[zrbf_key]["type"] == 0:
                zrbf_order_list.insert(0, zrbf_key)
                zrbf_pjxs_list.insert(0, IndicatorDic.zrbf[zrbf_key]["pjxs"])
            else:
                zrbf_order_list.append(zrbf_key)
                zrbf_pjxs_list.append(IndicatorDic.zrbf[zrbf_key]["pjxs"])

        if zrbf_keys:
            zrbf_keys.append("名称")
        for _ in data.keys():
            year_data = data[_]

            zb_tmp = pd.DataFrame(year_data.loc[:, zrbf_keys]).set_index('名称')
            zb_tmp = zb_tmp[zrbf_order_list]
            column_len = zb_tmp.shape[1]
            column_re_inx = [x for x in range(column_len)]
            zb_tmp.columns = column_re_inx
            data_cal[_] = zb_tmp
        zrbf_result = {"data": data_cal, "pjxs" : zrbf_pjxs_list}
        return zrbf_result

    @staticmethod
    def every_year_rp_data(data: Dict[str, pd.DataFrame]):
        """
        将奖惩数据组织成，年度，维度的数据
        :param data:
        :return:
        """
        dim_dic_keys = IndicatorDic.dim_dic.keys()  # all dim key
        data_y_keys = data.keys()
        data_cal = {}  # {年度：{维度：指标数据}}
        for _ in data_y_keys:
            year_data = data[_]
            year_dim_data = {}
            for dim_item in dim_dic_keys:
                dim_indicators = IndicatorDic.dim_dic[dim_item]  # 获取当前维度指标项
                year_dim_data[dim_item] = year_data.loc[:, dim_indicators['jc_indicator']]
            data_cal[_] = year_dim_data
        return data_cal
