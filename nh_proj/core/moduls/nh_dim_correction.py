# -*- coding: utf-8 -*-
"""
@Time    :  2020/12/5
@Author  : xinghen
@File    : nh_dim_correction.py
"""
import pandas as pd

from core.data_processing.indicator_dic import IndicatorDic


class NhDimCorrection:

    def reward_punish_correct(self, gra_data, rp_data):
        """
        对原始数据进行奖惩修正
        :param gra_data:
        :param rp_data:
        :return:
        """
        rp_correct_result = pd.DataFrame()
        for _dim in gra_data:
            rp_dim_data = rp_data.get(_dim)  # type: pd.DataFrame
            gra_dim_data = gra_data[_dim]  # type: pd.DataFrame
            if rp_dim_data is not None and not rp_dim_data.empty:
                gra_rp_data = pd.concat([gra_dim_data, rp_dim_data], ignore_index=True, axis=1)  # type: pd.DataFrame
                dim_sum_data = gra_rp_data.apply(lambda x: x.sum(), axis=1)
                rp_correct_result[_dim] = dim_sum_data
            else:
                rp_correct_result[_dim] = gra_dim_data
        rp_correct_result_normal = rp_correct_result.apply(lambda x: x / x.max(), axis=0)  # 归一化
        return rp_correct_result_normal

    def dea_correct(self, gra_data, dea_data):
        """
        数据包络算法计算自然禀赋系数
        :return:
        """
        dea_correct_result = pd.DataFrame()
        for dim_key in IndicatorDic.dim_dic.keys():
            if IndicatorDic.dim_dic[dim_key]["dea_flag"]:
                dea_correct_result[dim_key] = gra_data[dim_key] * dea_data.iloc[:, 0].astype("float")
            else:
                dea_correct_result[dim_key] = gra_data[dim_key]
        return dea_correct_result
