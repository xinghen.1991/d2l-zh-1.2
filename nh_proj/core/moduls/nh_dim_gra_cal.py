# -*- coding: utf-8 -*-
"""
@Time    :  2020/12/5
@Author  : xinghen
@File    : nh_dim_gra_cal.py
"""
from typing import Dict
import pandas as pd

from core.data_processing.indicator_dic import IndicatorDic
from core.utils.nh_gra_utils import NhGraUtils


class NhDimGraCal:

    @staticmethod
    def dim_gra_cal(y_data_values: pd.DataFrame, dim_weight):
        """
        计算各维度的灰色关联度系数数据
        :param data_call:
        :param dim_weight:
        :return:
        """
        dim_dic_keys = IndicatorDic.dim_dic.keys()  # all dim key
        gra_y_dim_result = {}
        for dim_key in dim_dic_keys:
            optimum_flag = IndicatorDic.dim_dic[dim_key]["optimum_flag"]
            y_dim_data = y_data_values[dim_key]
            y_dim_weight = dim_weight[dim_key]
            gra_result = NhGraUtils().gra(y_dim_data, optimum_flag, y_dim_weight)
            gra_y_dim_result[dim_key] = gra_result
        return gra_y_dim_result
