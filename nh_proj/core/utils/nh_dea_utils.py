# -*- coding: utf-8 -*-
"""
@Time    : 2020/11/20
@Author  : xinghen
@File    : nh_dea_utils.py
"""

from pyDEA.core.data_processing.parameters import parse_parameters_from_file, VALID_PARAM_NAME, VALID_PARAM_VALUE, \
    Parameters, VALID_PARAM_NAMES
from pyDEA.core.utils.run_routine import RunMethodTerminal
from pyDEA.core.utils.dea_utils import clean_up_pickled_files, get_logger
import re
import pandas as pd
import numpy as np
import calendar
import time
import os


class NhDeaUtils:

    def __init__(self):
        pass

    def min_max_range(self, x, range_values):  # 数据缩放
        return [
            round(((xx - min(x)) / (1.0 * (max(x) - min(x)))) * (range_values[1] - range_values[0]) + range_values[0],
                  2) for xx in x]

    def pre_dea(self, data, pjxs):
        pass
        data.fillna(0.01)
        data = pd.DataFrame(data=data, dtype=np.float64)  # 示例2
        for i in range(len(pjxs)):  # 将数据统一方向
            data.iloc[:, i] = data.iloc[:, i].pow(pjxs[i])
            data.iloc[:, i] = self.min_max_range(list(data.iloc[:, i]), (0, 100))
        return data

    def dea_py(self, pre_data, pjxs, base_file="/nh_proj/py_dea/"):

        data = self.pre_dea(pre_data, pjxs)
        filename = ""
        output_format = ""
        output_dir = ""
        sheet_name_usr = ""
        output_file = "pydea_o_"
        input_file = "pydea_data/"
        data_inx = data.index.values  # dea 数据的索引列
        data = data.reset_index()
        data = data.drop(labels="名称", axis=1)
        # 首先创建文件路径
        NhDeaUtils.dea_mkdir(base_file + input_file)
        efs = "EfficiencyScores.csv"  # dea crs score

        param_def = ["<OUTPUT_FILE> {e:/haha3}",
                     "<MULTIPLIER_MODEL_TOLERANCE> {0}",
                     "<CATEGORICAL_CATEGORY> {}",
                     "<WEAKLY_DISPOSAL_CATEGORIES> {}",
                     "<USE_SUPER_EFFICIENCY> {}",
                     "<VIRTUAL_WEIGHT_RESTRICTIONS> {}",
                     "<ABS_WEIGHT_RESTRICTIONS> {}",
                     "<OUTPUT_CATEGORIES> {o0}",
                     "<DATA_FILE> {E:/dea_test3.xlsx}",
                     "<NON_DISCRETIONARY_CATEGORIES> {}",
                     "<MAXIMIZE_SLACKS> {}",
                     "<INPUT_CATEGORIES> {i1; i2; i3; i4; i5; i6; i7; i8}",
                     "<DEA_FORM> {env}",
                     "<PRICE_RATIO_RESTRICTIONS> {}",
                     "<RETURN_TO_SCALE> {CRS}",
                     "<PEEL_THE_ONION> {}",
                     "<ORIENTATION> {input}"]

        logger = get_logger()
        logger.info('Params file "%s", output format "%s", output directory "%s", sheet name "%s".',
                    filename, output_format, output_dir, sheet_name_usr)

        pattern = ''.join(['<(', VALID_PARAM_NAME, r')>\s*{(',
                           VALID_PARAM_VALUE, ')}'])
        params = Parameters()
        df = pd.DataFrame(data)
        df.rename(columns=lambda x: 'o' + str(x) if x == 0 else 'i' + str(x), inplace=True)
        ts = calendar.timegm(time.gmtime())

        df.to_excel(base_file + input_file + str(ts) + ".xlsx")

        nb_parsed_params = 0
        for line in param_def:
            matched_params = re.findall(pattern, line)
            for match in matched_params:
                if match[0] in VALID_PARAM_NAMES:
                    params.update_parameter(match[0], match[1])
                    nb_parsed_params += 1

        params.update_parameter("OUTPUT_FILE", base_file + output_file + str(ts) + ".csv")  # 设置一个时间戳的随机值作为输出文件路径
        params.update_parameter("DATA_FILE", base_file + input_file + str(ts) + ".xlsx")  # 设置一个时间戳的随机值，作为输入文件路径
        # params.update_parameter("OUTPUT_CATEGORIES", "")  # 输出参数列名
        input_columns = ''
        for i in df.columns:
            if i == "o0":
                continue
            input_columns += (i + ";")

        params.update_parameter("INPUT_CATEGORIES", input_columns)  # 输入参数列名

        params.print_all_parameters()
        run_method = RunMethodTerminal(params, sheet_name_usr, output_format,
                                       output_dir)
        run_method.run(params)
        clean_up_pickled_files()
        df_efs = pd.read_csv(base_file + output_file + str(ts) + "/" + efs)

        logger.info('pyDEA exited.')
        df_efs = df_efs.drop(0, axis=0)
        df_efs = df_efs.set_index(data_inx).drop(labels="input orientation", axis=1)
        return df_efs

    def dea_mkdir(path):  # path是指定文件夹路径
        if os.path.isdir(path):
            # print('文件夹已存在')
            pass
        else:
            os.makedirs(path)


if __name__ == '__main__':
    df = pd.read_excel("E:/dea_test2.xlsx")
    print(NhDeaUtils().dea_py(df.values))
