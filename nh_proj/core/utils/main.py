# -*- coding: utf-8 -*-
"""
Created on Sat Jul 27 21:36:55 2019

@author: lenovo
"""
import numpy as np
import xlrd
import pandas as pd

# 权重
w = np.array([0.1806,0.1810,0.3064,0.332])

datafile = 'data.xlsx'
resultfile = 'result.xlsx'

# 从excel文件中读取数据
def read(file):
    wb = xlrd.open_workbook(filename=file)  # 打开文件
    sheet = wb.sheet_by_index(0)  # 通过索引获取表格
    rows = sheet.nrows  # 获取行数
    all_content = []  # 存放读取的数据
    for j in range(1, 5):  # 取第1~第4列对的数据
        temp = []
        for i in range(1, rows):
            cell = sheet.cell_value(i, j)  # 获取数据
            temp.append(cell)
        all_content.append(temp)  # 按列添加到结果集中
        temp = []
    return np.array(all_content)

def temp2(datas):
    K = np.power(np.sum(pow(datas,2),axis = 0),0.5)
    for j in range(K.size):
        datas[: , j] = datas[: , j] / K[j]
    return datas


def temp3(sta_data):
    z_max = np.amax(sta_data , axis=0)
    z_min = np.amin(sta_data , axis=0)
    # 计算每一个样本点与最大值的距离
    tmpmaxdist = np.power(np.sum(np.multiply(np.power((z_max - sta_data) , 2) , w) , axis = 1) , 0.5)  # 每个样本距离Z+的距离
    tmpmindist = np.power(np.sum(np.multiply(np.power((z_min - sta_data) , 2) , w) , axis = 1) , 0.5)  # 每个样本距离Z+的距离
    score = tmpmindist / (tmpmindist + tmpmaxdist)
    score = score / np.sum(score)  # 归一化处理
    return score

def main():

    answer1 = read(datafile)  # 读取文件，4*16
    answer2 = np.array(answer1).transpose()  # 将list转换为numpy数组并转置 16*4
    answer3 = temp2(answer2)  # 数组正向化 16*4
    answer4 = temp3(answer3)  # 标准化处理去钢  16*4 ， 4，
    data = pd.DataFrame(answer4)  # 计算得分

    # 将得分输出到excel表格中
    writer = pd.ExcelWriter(resultfile)  # 写入Excel文件
    data.to_excel(writer, 'page_1', float_format='%.5f')  # ‘page_1’是写入excel的sheet名
    writer.save()
    writer.close()


main()

