from __future__ import unicode_literals

import pandas as pd
from pyDEA.core.utils.dea_utils import get_logger

from core.data_processing.indicator_dic import IndicatorDic
from core.data_processing.processing_year_data import NhDataProcessing
from core.data_processing.read_data_from_xls import NhDataReader
from core.moduls.nh_dim_correction import NhDimCorrection
from core.moduls.nh_dim_gra_cal import NhDimGraCal
from core.moduls.nh_dim_weight_cal import NhDimWeightCal
from core.utils.nh_dea_utils import NhDeaUtils
from core.utils.nh_topsis_utils import NhTopsisUtil
import json


def Main():
    logger = get_logger()
    logger.info('=============  start  cal  =====================')
    year = '2019'  # 需要计算数据的年份
    correct_method = IndicatorDic.correct_method  # 修正方式
    all_year_data = NhDataReader().read_data_excel()  # all year's data
    connect_year_data = NhDataProcessing.connect_year_data(all_year_data)  # 每年度的加上向前推两年的数据

    #  各维度修正权重未进行计算
    all_year_dim_w = {}
    if correct_method == IndicatorDic.MULTIPLE_CORRELATION_W:  # 复相关算法求解权重系数
        all_year_dim_w = NhDimWeightCal().multiple_correlation_cal(connect_year_data)
    else:  # 变异算法计算权重系数
        all_year_dim_w = NhDimWeightCal().variable_coefficient_cal(connect_year_data)
    # 各维度的gra结果
    every_year_data = NhDataProcessing.every_year_data(all_year_data, "check_indicator")  # 每年度的数据
    every_year_rp_data = NhDataProcessing.every_year_data(all_year_data, "jc_indicator")  # 每年度的奖惩数据

    dim_gra_result = NhDimGraCal().dim_gra_cal(every_year_data[year], all_year_dim_w[year])  # year的各维度的gra结果
    rp_correct_result = NhDimCorrection().reward_punish_correct(dim_gra_result, every_year_rp_data[year])  # 奖惩修正结果

    zrbf_data = NhDataProcessing().every_year_zrbf_data(all_year_data)  # 自然禀赋指标数据
    dea_result = NhDeaUtils().dea_py(zrbf_data["data"][year], zrbf_data["pjxs"])  # 自然禀赋指标结果
    dea_correct_result = NhDimCorrection().dea_correct(dim_gra_result, dea_result)  # 自然禀赋修正结果
    db_correct_result = NhDimCorrection().dea_correct(rp_correct_result, dea_result)  # 双重修正结果

    dim_db_correct_cal = NhDimWeightCal().dim_db_correct_cal(all_year_data, all_year_dim_w, zrbf_data)  # 各维度计算的权重系数

    dim_topsis_cal_result = {'ori_result': NhTopsisUtil().topsis_cal(dim_gra_result, dim_db_correct_cal),
                             'rp_result': NhTopsisUtil().topsis_cal(rp_correct_result, dim_db_correct_cal),
                             'dea_result': NhTopsisUtil().topsis_cal(dea_correct_result, dim_db_correct_cal),
                             'db_result': NhTopsisUtil().topsis_cal(db_correct_result, dim_db_correct_cal)}

    np_result = {}

    for r_key in dim_topsis_cal_result.keys():
        _df = dim_topsis_cal_result[r_key].reset_index()
        np_result[r_key] = _df.values.tolist()

    return json.dumps(np_result, ensure_ascii=False)

    # print(all_year_dim_w)


if __name__ == '__main__':
    # df = pd.read_excel("E:/dea_test2.xlsx")
    # print(DeaUtils.dea_py(df.values))

    Main()
