import d2lzh as d2l
from mxnet.gluon import nn
from mxnet.gluon.nn import Conv2D, MaxPool2D
from mxnet import init, gluon, nd

def vgg_block(num_convs, num_channels):
    blk = nn.Sequential()
    for _ in range(num_convs):
        blk.add(
            Conv2D(num_channels, kernel_size=3, padding=1, strides=1, activation='relu')
        )
    blk.add(
        MaxPool2D(pool_size=2, strides=2)
    )
    return blk


pass
